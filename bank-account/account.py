"""BankAccount Application

This is an example demostrating how to create classes and object. It also
creating constructors, calling class variables, methods and functions
"""

# import
import random


class BankAccount():
    """Bank Account Class. """

    def __init__(self, name, initial_deposit):
        """BankAccount Class contructor

        This method is called whenever a new object is created.

        Arguments:
            name {string} -- Name of the account owner
            initial_deposit {int} -- Initial deposit amount
        """

        self.minimum_balance = 50
        self.account_balance = initial_deposit
        self.account_name = name
        self.account_number = random.randint((99999**2), (99999**3))

    def deposit(self, amount):
        """Increase the account balance with an amount

        Arguments:
            amount {int} -- amount to deposit

        Returns:
            int -- account balance + amount
        """

        self.account_balance += amount
        return self.account_balance

    def withdraw(self, amount):
        """Decreases the account balance with an amount

        Arguments:
            amount {int} -- amount to withdraw

        Returns:
            int -- account balance - amount
        """

        if self.account_balance <= self.minimum_balance:
            return "Reached minimum account balance GHS50"

        if (self.account_balance - amount) < self.minimum_balance:
            return "Reached minimum account balance: GHS50"

        self.account_balance -= amount

        return self.account_balance

    def account_details(self):
        """Returns accoutn detaisl. """

        details = "=====================================================\n"
        details += "=================== Account Details =================\n"
        details += "Account Name: " + self.account_name + "\n"
        details += "Account Number: " + str(self.account_number) + "\n"
        details += "Account Balance: GHS" + str(self.account_balance) + "\n"
        return details


# creates BankAcount Object
Ama = BankAccount("Ama Yeboah", 5000)

# deposite an amount
Ama.deposit(500)

# withdraws an amount
Ama.withdraw(600)

# deposite an amount
Ama.deposit(500)


# get account details
print(Ama.account_details())

# calls class variable (account_balance)
print("Class variable (self.account_balance):", Ama.account_balance)
